namespace AzureBusinessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Comment1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comment", "Name", c => c.String());
            AddColumn("dbo.Comment", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comment", "Email");
            DropColumn("dbo.Comment", "Name");
        }
    }
}
