﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureBusinessLayer.Models
{
    public class Comment
    {
        public int CommentId { get; set; }

        public int PostId { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public virtual Post Post { get; set; }
    }
}
