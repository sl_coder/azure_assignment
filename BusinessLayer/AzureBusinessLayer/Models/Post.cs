﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureBusinessLayer.Models
{
   public class Post
    {
        public int PostId { get; set; }

        public string Name { get; set; }

        public string Text { get; set; }

        [Display(Name = "Created User")]
        public string CreatedBy { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        [NotMapped]
        public string Comment { get; set; }

        [NotMapped]
        public string CreatedUserName { get; set; }

        [NotMapped]
        public string CreatedUserEmail { get; set; }
    }
}
