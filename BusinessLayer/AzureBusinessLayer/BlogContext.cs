﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AzureBusinessLayer.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace AzureBusinessLayer
{
    public class BlogContext:DbContext
    {
        public BlogContext() : base("BlogContext")
        {
        }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public DbSet<UserTokenCache> UserTokenCacheList { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
